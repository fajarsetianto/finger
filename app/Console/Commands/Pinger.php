<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Device;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Events\Device\ConnectionChangedEvent;
use App\Events\Device\LatencyUpdatedEvent;

class Pinger extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ping:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $devices = Device::active()->get();

        foreach($devices as $device){
            $latency = $this->getLatency($device->ip);
            $isOnline = !is_bool($latency);
            $cacheKey = 'device.'.$device->id.'status';
            $currentStatus = Cache::get($cacheKey) ?? false;
            $currentStatus = $device->is_online;
            Log::info('ping '.$device->ip, ['latency' => $latency, 'current_status' => $currentStatus]);
            
            if($currentStatus != $isOnline){
                $device->update(['is_online' => $isOnline]);
                event(new ConnectionChangedEvent($isOnline, $device, $latency));
            }

            if($isOnline){
                event(new LatencyUpdatedEvent($latency, $device));
            }
        }
       
    }

    protected function getLatency($ip){
        $latency = false;

        if (PHP_OS_FAMILY === "Windows") {
            exec("ping -w 1000 -n 1 " . $ip , $output, $result);
        }else{
            exec("ping -c 1 -W 2 " . $ip , $output, $result);
        }

        $commandOutput =  implode('', $output);
        $output = array_values(array_filter($output));
        if (!empty($output[1])) {
            // Search for a 'time' value in the result line.
            $response = preg_match("/time(?:=|<)(?<time>[\.0-9]+)(?:|\s)ms/", $output[1], $matches);
    
            // If there's a result and it's greater than 0, return the latency.
            if ($response > 0 && isset($matches['time'])) {
                $latency = round($matches['time'], 4);
            }
        }
        return $latency;
    }
}
