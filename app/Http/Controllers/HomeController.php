<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Device;

class HomeController extends Controller
{
    public function index(){
        $devices = Device::active()->get();
        return view('home',compact('devices'));
    }
}
