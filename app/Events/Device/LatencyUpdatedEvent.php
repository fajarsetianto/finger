<?php

namespace App\Events\Device;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LatencyUpdatedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     */
    public $latency, $device_id, $time;

    public function __construct($latency, $device)
    {
        $this->latency = $latency;
        $this->device_id = $device->id;
        $this->time = Carbon::now()->format('d/m/Y h:i A');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new Channel('device')
        ];
    }

    public function broadcastAs() {
        return 'Device.LatencyUpdatedEvent';
    }
}
