<?php

namespace App\Events\Device;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Carbon\Carbon;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ConnectionChangedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     */
    public $status, $device, $time, $latency;

    public function __construct($status, $device, $latency = null)
    {
        $this->status = $status;
        $this->device = $device->only('id','lokasi','kode_pt','is_online');
        $this->latency = $latency;
        $this->time = Carbon::now()->format('d/m/Y h:i A');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new Channel('device')
        ];
    }

    public function broadcastAs() {
        return 'Device.ConnectionChangedEvent';
    }
}
