<?php

namespace App\Repositories;

use App\Models\Device;
use Carbon\Carbon;

interface DeviceInterfaceRepository{
    public function getTransactions(Device $device, Carbon $startTime, Carbon $endTime = null );

    public function getDeviceInfo(Device $device);

    // public function checkConnection(): boolean;
}