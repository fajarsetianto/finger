<?php 

namespace App\Repositories;

use Carbon\Carbon;
use App\Models\Device;
use App\Models\AbsensiRaw;
use Illuminate\Support\Facades\Http;

class ZktecoRepository implements DeviceInterfaceRepository{

    public function getTransactions(Device $device, Carbon $startTime, Carbon $endTime = null ){
        try{
            $params = [];
            $params['url'] = $device->ip.':'.$device->port;
            $params['token'] = $this->getToken($params);
            $params['page'] = 1;
            $params['page_size'] = '30';
            $params['terminal_sn'] = $device->serial_number;
            $params['start_time'] = $startTime->format('Y-m-d') . ' 00:00:00';
            $params['end_time'] = ($endTime == null ? $startTime->format('Y-m-d') : $endTime->format('Y-m-d')).' 23:59:59';
            
            $data = collect();

            $loop = true;
            while($loop){
                $currentTransactions = $this->fetch($params);
                if($currentTransactions->collect()['next'] != null){
                    $params['page']++;
                }
                else{
                    $loop = false;
                }

                $data->push(...$currentTransactions->collect()['data']);
            }

            $data = $data->groupBy('emp_code');
            $data = $data->map(function($group, $groupName) {
                $masuk = $group->firstWhere('punch_time', $group->min('punch_time'));
                $masuk['is_masuk'] = true;
                $pulang = $group->firstWhere('punch_time', $group->max('punch_time'));
                $pulang['is_masuk'] = false;

                if($masuk['id'] == $pulang['id']){
                    $punch_time = Carbon::createFromFormat('Y-m-d H:i:s', $masuk['punch_time']);   
                    if($punch_time->lt($punch_time->format('Y-m-d')." 12:00:00")){
                        $pulang = null;
                    }else{
                        $masuk = null;
                    }
                }
                $output = collect();
                $output = $masuk != null ? $output->push($masuk) : $output;
                $output = $pulang != null ? $output->push($pulang) : $output;

                return $output;
            });
            
            $data = $data->flatten(1);

            $data = $data->map(function($item) use ($device){
                $punchTime = Carbon::createFromFormat('Y-m-d H:i:s', $item['punch_time']);
                return new AbsensiRaw([
                    'device_id' => $device->id,
                    'lokasi_pt' => $device->lokasi_pt,
                    'pin' => $item['emp'],
                    'date' => $punchTime->format('Y-m-d'),
                    'time' => $punchTime->format('H:i:s'),
                    'sn' => $item['terminal_sn'],
                    'verify_mode' => $item['verify_type'],
                    'work_code'=> $item['work_code'],
                    'io_mode' => $item['verify_type'],
                    'is_masuk' => $item['is_masuk']
                ]);
            });

            return $data;
            
        }catch(e){

        }
    }

    public function getDeviceInfo(Device $device){
        return true;
    }

    public function fetch($params){
        
        $uri = "/iclock/api/transactions/";

        $request = Http::withHeaders([
                        'Authorization' => 'JWT '.$params['token'],
                    ])
                    ->timeout(50)
                    ->baseUrl($params['url'])
                    ->get($uri,$params);
        return $request;
    }

    public function getToken($params){
        $params['username'] = 'admin';
        $params['password'] = '12345678!';
        $uri = "/jwt-api-token-auth/";

        $request = Http::timeout(50)
                    ->baseUrl($params['url'])
                    ->asForm()
                    ->post($uri,$params);
                    
        if($request->successful()){
            return $request->collect()['token'];
        }
        return null;
    }

}