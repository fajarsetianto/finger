<?php 

namespace App\Repositories;

use App\Models\Device;
use App\Models\AbsensiRaw;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

class FingerspotRepository implements DeviceInterfaceRepository{

    public function getTransactions(Device $device, Carbon $startTime, Carbon $endTime = null ){
        $params['sn'] = $device->serial_number;
        $params['base_url'] = $device->ip.':'.$device->port;
        $params['uri'] = 'scanlog/all/paging';
        $params['limit'] = 100;

        $data = collect();
        $session = true;
        $result = true;
        while($session && $result){
            $response = $this->fetch($params);
            if($response->successful()){
                $currentData = collect($response->collect()['Data']);
                $currentData = $currentData->filter(
                    function($item) use ($startTime, $endTime){
                        $date = Carbon::createFromFormat('Y-m-d H:i:s',$item['ScanDate']);
                        return $endTime != null 
                            ? $date->between($startTime->startOfDay(), $endTime->endOfDay(), true)
                            : $date->between($startTime->copy()->startOfDay(), $startTime->copy()->endOfDay(), true);
                    }
                );
                $data->push(...$currentData);
                $session = $response['IsSession'];
            }else{

            }
        }

        $data = $data->groupBy('PIN');

        $data = $data->map(function($group, $groupName) {
            $masuk = $group->firstWhere('ScanDate', $group->min('ScanDate'));
            $masuk['is_masuk'] = true;
            $pulang = $group->firstWhere('ScanDate', $group->max('ScanDate'));
            $pulang['is_masuk'] = false;

            if($masuk['ScanDate'] == $pulang['ScanDate']){
                $punch_time = Carbon::createFromFormat('Y-m-d H:i:s', $masuk['ScanDate']);   
                if($punch_time->lt($punch_time->format('Y-m-d')." 12:00:00")){
                    $pulang = null;
                }else{
                    $masuk = null;
                }
            }
            $output = collect();
            $output = $masuk != null ? $output->push($masuk) : $output;
            $output = $pulang != null ? $output->push($pulang) : $output;

            return $output;
        });

        $data = $data->flatten(1);

        $data = $data->map(function($item) use ($device){
            $punchTime = Carbon::createFromFormat('Y-m-d H:i:s', $item['ScanDate']);
            return new AbsensiRaw([
                'device_id' => $device->id,
                'lokasi_pt' => $device->lokasi_pt,
                'pin' => trim($item['PIN']),
                'date' => $punchTime->format('Y-m-d'),
                'time' => $punchTime->format('H:i:s'),
                'sn' => $item['SN'],
                'verify_mode' => $item['VerifyMode'],
                'work_code'=> $item['WorkCode'],
                'io_mode' => $item['IOMode'],
                'is_masuk' => $item['is_masuk'],
            ]);
        });

        return $data;

    }

    public function getDeviceInfo(Device $device){
        $params['sn'] = $device->serial_number;
        $params['base_url'] = '192.168.12.99:'.$device->port;
        $params['uri'] = 'dev/info';
        $response = $this->fetch($params);
        if($response->successful()){
            return $response->collect()['Result'];
        }
        return false;
    }

    protected function fetch($params){
        $request = Http::timeout(50)->baseUrl($params['base_url'])
                    ->asForm()
                    ->post($params['uri'] ,$params);
        return $request;
    }
}