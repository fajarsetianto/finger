@extends('layouts.app')

@section('content')
<div class="flex flex-col">
    <header class="flex w-full bg-gray-100 relative">
        <div>0</div>
        <div class="flex-grow flex justify-end">0</div>
    </header>
    <main class="flex-1 flex flex-col p-3 bg-gray-50">
        <div class="flex w-full">
            <div>
                <div class="relative">
                    <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                        <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                        </svg>
                    </div>
                    <input class="block w-full p-2 ps-10 text-sm text-gray-900 border border-gray-100 rounded-lg ring-offset-0 bg-gray-100 dark:focus:ring-blue-500 dark:focus:border-blue-200"  placeholder="Search devices, Location...">
                </div>
            </div>
            <div class="justify-end flex-grow flex">
                <div class="w-8 h-8 rounded-md hover:bg-gray-100 flex items-center justify-center">
                    <span class="icon-[mdi--grid-large] text-gray-500"></span>
                </div>
                <div class="w-8 h-8 rounded-md hover:bg-gray-100 flex items-center justify-center">
                    <span class="icon-[mdi--filter-multiple-outline] text-gray-500"></span>
                </div>
            </div>
        </div>
        <div class="container mx-auto w-screen mt-4 md:max-w-screen-lg">
            <div class="grid grid-cols-2 gap-x-6 gap-y-6 sm:grid-cols-4 mt-3 px-4">
                @each('components.device.card', $devices, 'device')
            </div>
    </div>
    </main>
</div>

    
    

@endsection
@push('scripts')
    <script type="module">
        Echo.channel('device')
            .listen('DeviceEvent', (data) => {
                var event = new CustomEvent('toast-request', {detail: data})
                window.dispatchEvent(event)
                var event = new CustomEvent('update-device-'+data.device.id+'-request', {detail: data.device})
                window.dispatchEvent(event)
            })
            .listen('.Device.LatencyUpdatedEvent', (data) => {
                window.dispatchEvent((new CustomEvent('device-'+data.device_id+'-updated-latency', {detail: data})))
            })
            .listen('.Device.ConnectionChangedEvent', (data) => {
                window.dispatchEvent((new CustomEvent('toast-request', {detail: {...data, 'eventType' : 'connection'}})))
                window.dispatchEvent((new CustomEvent('device-'+data.device.id+'-updated-connection', {detail: data})))
            });
    </script>
@endpush