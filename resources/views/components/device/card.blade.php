
<div
    x-data="{
        device: null,
        latency: null
    }"
    x-init="device = {{$device->toJson()}}"
    @device-{{$device->id}}-updated-connection.window="device = {...device, ...$event.detail.device};"
    
    @device-{{$device->id}}-updated-latency.window="latency = $event.detail.latency;"

    class="border border-1 p-2 rounded-md hover:shadow-2xl hover:bg-gray-10 ">
        <div class="h-40 card-bg-zkteco bg-center bg-cover"></div>
        <div class="divide-y divide-solid">
            <div class="flex w-full flex-col items-center py-2">
                <span class="rounded-md text-sm font-bold mb-1" x-text="device.lokasi"></span>
                <div class="text-xs items-center w-full">
                    <template x-if="device.is_online">
                        <div class="flex justify-between">
                            <div>
                                <span class="relative inline-flex rounded-full h-2 w-2 bg-green-500"></span>
                                <span class="text-gray-300"> Online</span>
                            </div>
                            <template x-if="latency == null">
                                <div class="h-3 bg-gray-200 rounded-full dark:bg-gray-700 w-6 animate-pulse"></div>
                            </template>
                            <template x-if="latency != null">
                                <div class="flex items-center">
                                    <span class="icon-[mdi--timer-sand] text-gray-500"></span>
                                    <span class="text-gray-300" x-text="latency + ' ms'"></span>
                                </div>
                            </template>
                        </div>
                    </template>
                    <template x-if="!device.is_online">
                        <div class="flex justify-between">
                            <span class="relative inline-flex rounded-full h-2 w-2 bg-red-500"></span>
                            <span class="text-gray-300"> Offline</span>
                        </div>
                        <div class="flex items-center">
                                <span class="icon-[mdi--timer-sand] text-gray-500"></span>
                                <span class="text-gray-300"> - </span>
                            </div>
                    </template>
                </div>
            </div>
            <div class="py-3">
                <div class="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
                    <div class="bg-green-500 h-2.5 rounded-full" style="width: 45%"></div>
                </div>
                <div class="flex justify-between text-[10px] text-gray-300">
                    <span>1</span>
                    <span>100</span>
                </div>
            </div>
            <div class="py-3">
                <div class="w-10 h-10 rounded-md hover:bg-gray-100 flex items-center justify-center">
                    <span class="icon-[mdi--filter-multiple-outline] text-gray-500"></span>
                </div>
            </div>
        </div>
    </div>