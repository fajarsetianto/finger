<div
    x-data="{
        messages: [],
        remove(message) {
            this.messages = this.messages.filter(i => i.id != message.id)
        },
    }"
    @toast-request.window="
        let message = $event.detail;
        message = {id : Date.now(), ...message}
        messages.unshift(message);"
    class="fixed inset-0 z-50 flex flex-col items-end justify-start px-4 py-6 space-y-4 pointer-events-none sm:p-6"
>
    <template x-for="(message, messageIndex) in messages" :key="message.id" hidden>
        <div
            x-data="{
                show: false,
                timeout: null,
                setTimeoutMessage(){
                    this.timeout = setTimeout( () => {
                        this.show = false;
                        setTimeout(() => remove(message) ,300)
                    }, 2500)
                }
            }"
            x-show="show"
            x-init = "$nextTick(() => show = true);setTimeoutMessage();"
            x-transition:enter="!transition !ease-out !duration-300"
            x-transition:enter-start="!opacity-0 !scale-0 -translate-y-[100%] blur-lg "
            x-transition:enter-end="!opacity-100 !scale-100 translate-y-0 blur-none"
            x-transition:leave="!transition !ease-in !duration-300"
            x-transition:leave-start="!opacity-100 !scale-100 translate-y-0"
            x-transition:leave-end="!opacity-0 !scale-0 -translate-y-[100%]"
            @mouseenter="clearTimeout(timeout);"
            @mouseleave="setTimeoutMessage()"
            
            class="w-full md:max-w-sm bg-white rounded-lg shadow-lg pointer-events-auto"
        >
            <div :class="{
                    'ring-red-500': message.type === 'error',
                    'ring-blue-500': message.type === 'info',
                    'ring-yellow-500': message.type === 'warning',
                }"
                class="w-full  overflow-hidden bg-white rounded-lg shadow-lg pointer-events-auto border border-gray-200"
            >
                <template x-if="message.eventType == 'connection'">
                    @include('components.notification.device.connection')
                </template>
                <template x-if="message.eventType == 'syncronizing'">
                    @include('components.notification.device.syncronizing')
                </template>
            </div>
        </div>
    </template>
</div>
