<div  class="flex items-center w-full p-4 text-gray-500 bg-white rounded-lg shadow dark:text-gray-400 dark:bg-gray-800" role="alert">
    <div
        :class="{
            '!text-green-500 !bg-green-100:': message.status,
            '!text-red-500 !bg-red-100': !message.status
        }",
        class="inline-flex items-center justify-center flex-shrink-0 w-8 h-8 rounded-lg">
        <span :class="{ 'icon-[mdi--wifi]': message.status, 'icon-[mdi--wifi-off]': !message.status }"></span>
    </div>
    <div class="ms-3">
        <div class="text-sm font-semibold text-gray-900 dark:text-white">
            <template x-if="message.status">
                <div>Mesin terhubung.</div>
            </template>
            <template x-if="!message.status">
                <div>Mesin terputus.</div>
            </template>
        </div>
        <div class="grid grid-cols-2 gap-2 text-xs">
            <div class="flex items-center"><span class="icon-[mdi--map-marker-outline] text-gray-900 dark:text-white"></span> <span x-text="message.device.lokasi"></span></div>
            <div class="flex items-center"><span class="icon-[mdi--clock-time-five-outline] text-gray-900 dark:text-white"></span>  <span x-text="message.time"></span></div>
        </div>
    </div>
    
    <button type="button" @click="remove(message)" class="ms-auto -mx-1.5 -my-1.5 bg-white text-gray-400 hover:text-gray-900 rounded-lg focus:ring-2 focus:ring-gray-300 p-1.5 hover:bg-gray-100 inline-flex items-center justify-center h-8 w-8 dark:text-gray-500 dark:hover:text-white dark:bg-gray-800 dark:hover:bg-gray-700" data-dismiss-target="#toast-success" aria-label="Close">
        <span class="sr-only">Close</span>
        <span class="icon-[mdi--window-close]"></span>
    </button>
</div>