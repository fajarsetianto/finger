<div class="w-full max-w-sm bg-white dark:border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 bg-gradient-to-br from-indigo-500 from-10% via-sky-500 via-30% to-emerald-500 to-90%">
    <div class="sm:px-6 px-4 pt-4">
        <h5 class="text-base font-bold text-gray-900 md:text-xl dark:text-white">{{$device->brand->name}}</h5>
        <!-- <p class="text-sm font-normal text-gray-500 dark:text-gray-400">Connect with one of our available wallet providers or create a new one.</p> -->
    </div>
  
    <ul class="my-4 space-y-3 sm:p-6 p-4">
        <li>
            <a href="#" class="flex items-center p-3 text-base font-bold text-gray-900 rounded-lg bg-gray-50 hover:bg-gray-100 group hover:shadow dark:bg-gray-600 dark:hover:bg-gray-500 dark:text-white">
                <span class="icon-[mdi--map-marker]"></span>
                <span class="flex-1 ms-3 whitespace-nowrap md:text-sm">{{$device->lokasi_pt}}</span>
            </a>
        </li>
        <li>
            <a href="#" class="flex items-center p-3 text-sm font-bold text-gray-900 rounded-lg bg-gray-50 hover:bg-gray-100 group hover:shadow dark:bg-gray-600 dark:hover:bg-gray-500 dark:text-white">
                <span class="icon-[mdi--account-group]"></span>
                <span class="flex-1 ms-3 whitespace-nowrap">50 / 100</span>
            </a>
        </li>
        <li>
            <a href="#" class="flex items-center p-3 text-sm font-bold text-gray-900 rounded-lg bg-gray-50 hover:bg-gray-100 group hover:shadow dark:bg-gray-600 dark:hover:bg-gray-500 dark:text-white">
                <span class="icon-[mdi--fingerprint]"></span>
                <span class="flex-1 ms-3 whitespace-nowrap">200 / 20000</span>
            </a>
        </li>
    </ul> 
    <div class="sm:px-6 px-4 pb-4 pt-2 flex justify-between" >
        <span class="flex items-center p-3 text-base font-bold text-gray-900 rounded-lg bg-gray-50 group dark:bg-gray-600 dark:hover:bg-gray-500 dark:text-white">
            <svg xmlns="http://www.w3.org/2000/svg" id="online-indicator-{{$device->id}}" fill="{{ $device->is_online ? 'green' : 'red' }}" viewBox="0 0 24 24" stroke-width="1.5" stroke="{{ $device->is_online ? 'green' : 'red' }}" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="M3 13.125C3 12.504 3.504 12 4.125 12h2.25c.621 0 1.125.504 1.125 1.125v6.75C7.5 20.496 6.996 21 6.375 21h-2.25A1.125 1.125 0 0 1 3 19.875v-6.75ZM9.75 8.625c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125v11.25c0 .621-.504 1.125-1.125 1.125h-2.25a1.125 1.125 0 0 1-1.125-1.125V8.625ZM16.5 4.125c0-.621.504-1.125 1.125-1.125h2.25C20.496 3 21 3.504 21 4.125v15.75c0 .621-.504 1.125-1.125 1.125h-2.25a1.125 1.125 0 0 1-1.125-1.125V4.125Z" />
            </svg>
            <p class="text-sm">{{$device->ip}}</p>
        </span>
        <span class="flex items-center p-3 text-base font-bold text-gray-900 rounded-lg bg-gray-50 group dark:bg-gray-600 dark:hover:bg-gray-500 dark:text-white">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round" d="m11.25 11.25.041-.02a.75.75 0 0 1 1.063.852l-.708 2.836a.75.75 0 0 0 1.063.853l.041-.021M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Zm-9-3.75h.008v.008H12V8.25Z" />
            </svg>

        </span>
        <span class="flex items-center p-3 text-base font-bold text-gray-900 rounded-lg bg-gray-50 group dark:bg-gray-600 dark:hover:bg-gray-500 dark:text-white">
            <!-- <i class="fa-solid fa-wifi"></i> -->

            <span class="icon-[mdi--signal-cellular-outline]"></span>
        </span>
    </div>
</div>

@push('scripts')
    <script type="module">
        function changeOnlineIndicator(data){
            const onlineIndicator = document.getElementById('online-indicator-{{$device->id}}')
            const card = document.getElementById('card-of-{{$device->id}}')
            const color = data.message.status ? 'green' : 'red';
            const colorMode = data.message.status ? 'dark' : 'red';
            onlineIndicator.setAttribute("stroke", color);
            onlineIndicator.setAttribute("fill", color);
        }

        Echo.channel('public.device.{{$device->id}}')
            .listen('DeviceEvent', (data) => {
                changeOnlineIndicator(data);
            });
    </script>
@endpush