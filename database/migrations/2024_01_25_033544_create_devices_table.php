<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->id();
            $table->string('kode_pt');
            $table->string('lokasi');
            $table->string('ip');
            $table->string('image');
            $table->string('serial_number');
            $table->string('port');
            $table->unsignedBigInteger('brand_id');
            $table->boolean('is_active')->default(false);
            $table->string('series')->nullable();
            $table->boolean('is_online')->default(false);
            $table->boolean('is_synchronizing')->default(false);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('device');
    }
};
